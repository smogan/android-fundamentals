package com.deloittece.androidfundamentals.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.deloittece.androidfundamentals.R
import com.deloittece.androidfundamentals.model.RepoModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_repository.view.*

            /*
             * ### Step 9 ###
             */
class ReposAdapter(var repositories: List<RepoModel>/*, var clickListener: OnClickListener*/): RecyclerView.Adapter<ReposAdapter.RepoViewHolder>() {

     /*
      * ### Step 9 ###
      */
    /*interface OnClickListener {
        fun onRepoClicked(url: String)
    }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_repository, parent, false)
        return RepoViewHolder(parent.context, v)
    }

    override fun getItemCount(): Int {
        return repositories.size
    }

    override fun onBindViewHolder(holder: RepoViewHolder, position: Int) {
        val repo = repositories[position]

        Picasso.get()
            .load(repo.owner.avatar)
            .into(holder.imageView)

        holder.tvFullName.text = repo.fullName
        holder.tvDescription.text = repo.description
        holder.tvLanguage.text = holder.context.getString(R.string.label_language, repo.language)
        holder.tvForks.text = repo.forks.toString()
        holder.tvStars.text = repo.stars.toString()

        /*
         * ### Step 9 ###
         */
        /*holder.itemView.setOnClickListener {
            clickListener.onRepoClicked(repo.url)
        }*/
    }

    class RepoViewHolder(val context: Context, itemView: View): RecyclerView.ViewHolder(itemView) {
        val imageView = itemView.imageView
        val tvFullName = itemView.tvFullName
        val tvDescription = itemView.tvDescription
        val tvLanguage = itemView.tvLanguage
        val tvForks = itemView.tvForks
        val tvStars = itemView.tvStars
    }

}