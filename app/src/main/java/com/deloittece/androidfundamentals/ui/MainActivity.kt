package com.deloittece.androidfundamentals.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.deloittece.androidfundamentals.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
