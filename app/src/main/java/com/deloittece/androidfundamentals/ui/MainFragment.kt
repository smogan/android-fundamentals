package com.deloittece.androidfundamentals.ui


import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.deloittece.androidfundamentals.R
import com.deloittece.androidfundamentals.api.RestClient
import com.deloittece.androidfundamentals.data.GithubRepository
import kotlinx.android.synthetic.main.fragment_main.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.deloittece.androidfundamentals.Injection
import com.deloittece.androidfundamentals.adapter.ReposAdapter
import com.deloittece.androidfundamentals.model.RepoModel
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import androidx.navigation.fragment.findNavController


/**
 * A simple [Fragment] subclass.
 *
 */

        /*
         * ### Step 9 ###
         */
class MainFragment : Fragment()/*, ReposAdapter.OnClickListener*/ {

    /*
     * ### Step 6 ###
    */
    //private lateinit var viewModel: MainViewModel

    /*
     * ### Step 7 ###
     */
    //private lateinit var adapter: ReposAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /*
         * ### Step 6 ###
         */
        /*viewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory(requireContext()))
            .get(MainViewModel::class.java)*/
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /*
         * ### Step 7 ###
         */
        /*val decoration = DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
        list.setHasFixedSize(true)
        list.layoutManager = LinearLayoutManager(requireContext())
        list.addItemDecoration(decoration)*/

        /*
         * ### Step 8 ###
         */
        /*etSearch.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                etSearch.clearFocus()
                val `in` = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                `in`.hideSoftInputFromWindow(etSearch.windowToken, 0)
                viewModel.searchRepos(etSearch.text.toString(), 20)
            }
            false
        }*/

//        btnGoToDetails.setOnClickListener {
//            it.findNavController().navigate(R.id.action_mainFragment_to_detailsFragment)
//        }
//        btnGoToDetails.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.detailsFragment, null))

//        val gitRepo = GithubRepository(RestClient.create())
//        gitRepo.search("tetris", 20)
//
//        gitRepo.repos.observe(this, Observer {
//            Log.d("OBSERVED_OK", "asdf")
//        })
//
//        gitRepo.errors.observe(this, Observer {
//            Log.d("OBSERVED_ERR", it)
//        })

        /*
         * ### Step 8 ### - comment this line
         */
        /*
         * ### Step 6 ###
         */
        //viewModel.searchRepos("tetris", 20)

        /*
         * ### Step 6 ###
         */
        /*viewModel.repos.observe(this, Observer {
//            Log.d("OBSERVED_OK", "asdf")

            /*
             * ### Step 9 ### - uncomment "this" param
             */
            /*
             * ### Step 7 ###
             */
            //adapter = ReposAdapter(it/*, this*/)
            //list.adapter = adapter

            /*
             * ### Step 8 ###
             */
            //etSearch.setText("")

            /*
             * ### Step12 ###
             */
            //viewModel.showLoader.postValue(false)
        })*/

        /*
         * ### Step 6 ###
         */
        /*viewModel.netWorkErrors.observe(this, Observer {
            Log.d("OBSERVED_ERR", it)
            /*
             * ### Step 12 ###
             */
            //viewModel.showLoader.postValue(false)
        })*/

        /*
         * ### Step 12 ###
         */
        /*viewModel.showLoader.observe(this, Observer {
            if (it) {
                progressBar.visibility = View.VISIBLE
            } else {
                progressBar.visibility = View.GONE
            }
        })*/
    }

    /*
     * ### Step 9 ###
     */
    /*override fun onRepoClicked(url: String) {
        /*
         * ### Step 10 ### - commend this code
         */
        Toast.makeText(requireContext(), url, Toast.LENGTH_SHORT).show()

        /*
         * ### Step 10 ###
         */
        /*val direction = MainFragmentDirections.actionMainFragmentToDetailsFragment()
        direction.repoUrl = url
        findNavController().navigate(direction)*/
    }*/

}
