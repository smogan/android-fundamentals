package com.deloittece.androidfundamentals.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.deloittece.androidfundamentals.data.GithubRepository
import com.deloittece.androidfundamentals.model.RepoModel

class MainViewModel(private val repository: GithubRepository): ViewModel() {

    /*
     * ### Step12 ###
     */
    //var showLoader = MutableLiveData<Boolean>()

    val repos: LiveData<List<RepoModel>> = Transformations.switchMap(repository.searchResult) {
        it -> it.data
    }

    val netWorkErrors: LiveData<String> = Transformations.switchMap(repository.searchResult) {
            it -> it.networkErrors
    }

    fun searchRepos(query: String, itemsPerPage: Int) {
        repository.search(query, itemsPerPage)
        /*
         * ### Step12 ###
         */
        //showLoader.postValue(true)
    }

}