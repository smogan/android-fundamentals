package com.deloittece.androidfundamentals.ui


import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.widget.Toast
import com.deloittece.androidfundamentals.R
import kotlinx.android.synthetic.main.fragment_details.*
import android.webkit.WebView
import android.webkit.WebViewClient


/**
 * A simple [Fragment] subclass.
 *
 */
class DetailsFragment : Fragment() {

    /*
     * ### Step 11 ###
     */
    //var repoUrl = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /*
         * ### Step 10 ###
         */
        /*arguments.let {
            val detailArgs = DetailsFragmentArgs.fromBundle(arguments!!)

            /*
             * ### Step 11 ###
             */
            /*repoUrl = detailArgs.repoUrl
            Toast.makeText(requireContext(), repoUrl, Toast.LENGTH_SHORT).show()*/
        }*/
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /*
         * ### Step 12 ###
        */
        /*webView.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                Log.d("PROGRESS", progress.toString())
                progressBar?.progress = progress
            }
        }

        webView.webViewClient = object: WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                progressBar?.visibility = View.VISIBLE
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                progressBar?.visibility = View.GONE
                super.onPageFinished(view, url)
            }
        }

        webView.settings.javaScriptEnabled = true*/

        /*
         * ### Step 11 ###
         */
        //webView.loadUrl(repoUrl)
    }

}
