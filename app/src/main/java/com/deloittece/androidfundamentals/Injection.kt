package com.deloittece.androidfundamentals

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.deloittece.androidfundamentals.api.RestClient
import com.deloittece.androidfundamentals.data.GithubRepository
import com.deloittece.androidfundamentals.ui.ViewModelFactory

object Injection {

    /**
     * Creates an instance of [GithubRepository] based on the [RestClient]
     */
    private fun provideGithibRepository(context: Context): GithubRepository {
        return GithubRepository(RestClient.create())
    }

    /**
     * Provides the [ViewModelProvider.Factory] that is then used to get a reference to
     * [ViewModel] objects
     */
    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory {
        return ViewModelFactory(provideGithibRepository(context))
    }

}