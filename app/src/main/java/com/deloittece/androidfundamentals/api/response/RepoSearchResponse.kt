package com.deloittece.androidfundamentals.api.response

import com.deloittece.androidfundamentals.model.RepoModel
import com.google.gson.annotations.SerializedName

data class RepoSearchResponse (
    @SerializedName("total_count") val total: Int = 0,
    @SerializedName("items") val items: List<RepoModel> = emptyList()
)