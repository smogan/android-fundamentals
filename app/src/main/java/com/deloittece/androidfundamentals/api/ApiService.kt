package com.deloittece.androidfundamentals.api

import com.deloittece.androidfundamentals.api.response.RepoSearchResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("search/repositories?sort=stars")
    fun searchRepos(@Query("q") query: String,
                    @Query("per_page") itemsPerPage: Int): Single<RepoSearchResponse>

}