package com.deloittece.androidfundamentals.model

import com.google.gson.annotations.SerializedName

/**
 * Immutable model class for a Github repository
 */
data class RepoModel (
    @field:SerializedName("id") val id: Long,
    @field:SerializedName("full_name") val fullName: String,
    @field:SerializedName("description") val description: String?,
    @field:SerializedName("html_url") val url: String,
    @field:SerializedName("stargazers_count") val stars: Int,
    @field:SerializedName("forks_count") val forks: Int,
    @field:SerializedName("language") val language: String?,
    @field:SerializedName("owner") val owner: OwnerModel
)