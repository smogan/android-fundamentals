package com.deloittece.androidfundamentals.model

import androidx.lifecycle.LiveData

data class RepoSearchResult (
    val data: LiveData<List<RepoModel>>,
    val networkErrors: LiveData<String>
)