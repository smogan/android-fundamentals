package com.deloittece.androidfundamentals.model

import com.google.gson.annotations.SerializedName

class OwnerModel(
    @field:SerializedName("avatar_url") val avatar: String?
)