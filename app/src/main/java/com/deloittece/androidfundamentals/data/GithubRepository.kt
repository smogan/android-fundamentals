package com.deloittece.androidfundamentals.data

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.deloittece.androidfundamentals.api.ApiService
import com.deloittece.androidfundamentals.model.RepoModel
import com.deloittece.androidfundamentals.model.RepoSearchResult
import io.reactivex.schedulers.Schedulers

class GithubRepository(private val service: ApiService) {

//    val data = MutableLiveData<List<RepoModel>>()
//    val errors = MutableLiveData<String>()

    val searchResult = MutableLiveData<RepoSearchResult>()

    fun search(query: String, itemsPerPage: Int) {
        val data = MutableLiveData<List<RepoModel>>()
        val errors = MutableLiveData<String>()

        val result = service.searchRepos(query, itemsPerPage)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.single())
            .subscribe( {
                response ->
                    Log.d("SERVER_SUCCESS", response.total.toString())
                    data.postValue(response.items)
                    /*
                    * ### Step 6 ###
                    */
                    //searchResult.postValue(RepoSearchResult(data, errors))
            },
                { error ->
                    Log.d("SERVER_ERROR", error.message)
                    errors.postValue(error.message)
                    /*
                    * ### Step 6 ###
                    */
                    //searchResult.postValue(RepoSearchResult(data, errors))
                })
    }

}